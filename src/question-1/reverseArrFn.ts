// Made function and accept arrays of strings/numbers for simplification of ad hoc testing
function reverseArr(origArr: (string | number)[]): (string | number)[] {
    let reversedArr = origArr.slice()
    for (let i = 0, n = reversedArr.length - 1; i < n; i++) {
        reversedArr.splice(i, 0, reversedArr.pop())
    }
    return reversedArr
}

reverseArrTest() // tsc reverseArrFn.ts; node reverseArrFn.js

function reverseArrTest() {
    const testArrs: (string | number)[][] = [
        ["now", "is", "the", "time"],
        ["two", 2, "potato", "three", 3, "potato", "four", 4],
        [1.41, 2, 3.14159, 4]
    ]
    testArrs.forEach((arr, i) => {
        const arrCopy1 = arr.slice()
        const arrCopy2 = arr.slice()
        const passes: boolean = arrCopy1.reverse().join() === reverseArr(arrCopy2).join();
        console.log("Test#", i + 1, passes ? " PASS" : " FAIL")
    })
}